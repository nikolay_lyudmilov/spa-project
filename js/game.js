var gameController = (function () {
    function gameController() {
        $('.home_page').hide();
        $('.game_page').show();
        $('.contacts_page').hide();
        $('.sponsors').hide();
        $('.portfolio_page').hide();
        $('.pages_page').hide();
        $(".wrapper").css('border-bottom', "1px solid #e6e6e6");

        alert('Use up and down arrow to move and spacebar to shoot');

        var overlaps = (function () {
            function getPositions(elem) {
                var pos, width, height;
                pos = $(elem).position();
                width = $(elem).width();
                height = $(elem).height();
                return [
                    [pos.left, pos.left + width],
                    [pos.top, pos.top + height]
                ];
            }

            function comparePositions(p1, p2) {
                var r1, r2;
                r1 = p1[0] < p2[0] ? p1 : p2;
                r2 = p1[0] < p2[0] ? p2 : p1;
                return r1[1] > r2[0] || r1[0] === r2[0];
            }

            return function (a, b) {
                var pos1 = getPositions(a),
                    pos2 = getPositions(b);
                return comparePositions(pos1[0], pos2[0]) && comparePositions(pos1[1], pos2[1]);
            };
        })();

        $('#playGame').prop('disabled', false);

        if ((window).innerWidth <= 768) {
            $('.nav-btn').on('click', () => {
                if ($('#nav').prop('checked') == true) {
                    $('#playGame').show();

                    $('#playGame').prop('disabled', false);
                } else {
                    $('#playGame').prop('disabled', true);

                    $('#playGame').hide();


                }
            })
        }

        if ((window).innerWidth <= 768 && ($('#nav').prop('checked') == true)) {
            $('#playGame').hide();
        } else {
            $('#playGame').show();
        }


        $(window).on('resize', function () {
            $('#player').css('top', (innerHeight - $('#player').height()) / 2);
        });
        $('#player').css('top', (innerHeight - $('#player').height()) / 2);

        $('body').css('overflow-y', 'hidden');

        const ENEMY_IMAGE = 'assets/images/enemy.png';
        const BULLET_IMAGE = 'assets/images/bullet.png';
        if ((window).innerWidth <= 768){
            var ENEMY_SIZE = 40;
            var ENEMY_STEP = 1;
            var BULLET_SIZE = 20;
        } else {
            var ENEMY_SIZE = 70;
            var ENEMY_STEP = 5;
            var BULLET_SIZE = 40;
        }
        var ENEMY_STEP = 5;
        const SPAWN_TIME = 1000;
        const SPACE = 32;
        const BULLET_MOVE_X = 20;
        
        var setIntervalId;
        var i;
        var playerMove = ((innerHeight - $('#player').height()) / 2);
        var enemyList = [];

        function Enemy() {
            this.x = window.innerWidth;
            this.y = Math.floor(Math.random() * ((innerHeight - 400) - ENEMY_SIZE)) + 200;
            this.element = $(`<img src='${ENEMY_IMAGE}'/>`);
            $('.game_page').append(this.element);
            this.element.css('position', 'fixed');
            this.element.width(ENEMY_SIZE);
            this.element.css('top', this.y);
            this.element.css('left', this.x);
        }

        Enemy.prototype.die = function () {
            cancelAnimationFrame(this.moveId);
            this.element.remove();
        }

        Enemy.prototype.move = function () {
            this.x -= ENEMY_STEP;
            this.element.css('left', this.x);
            this.moveId = requestAnimationFrame(this.move.bind(this));

            if (this.x <= 0) {
                alert('You lost!');
                enemyList.forEach(z => z.die());
                enemyList.length = 0;
                clearInterval(setIntervalId);
                $('.nav-btn').css('cursor', 'pointer');
                $('#playGame').show();

                $('#playGame').prop('disabled', false);
            }
        }

        function Bullet() {
            this.x = $('#player').width();
            (window.innerWidth <= 768) ? ($('#nav').prop('checked') == true) ?
            this.y = +$('#player').css('top').slice(0, -2) + 295:
                this.y = +$('#player').css('top').slice(0, -2) + 55:
                this.y = +$('#player').css('top').slice(0, -2) + 80;

            this.element = $(`<img class="bullets" src='${BULLET_IMAGE}'/>`);
            $('body').append(this.element);
            this.element.css('position', 'fixed');
            this.element.height(BULLET_SIZE + "px");
            this.element.css('top', this.y);
            this.element.css('left', this.x + "px");
        }

        Bullet.prototype.move = function () {
            this.x += BULLET_MOVE_X;
            this.element.css('left', this.x);

            var enemy = enemyList.find(e => overlaps(this.element, e.element), this);
            if (enemy != null) {
                enemy.die();
                enemyList.splice(enemyList.findIndex(e => enemy == e), 1);
                this.element.remove();
                return;
            }


            if (this.x < innerWidth - BULLET_SIZE) {
                requestAnimationFrame(this.move.bind(this), 30);
            } else {
                this.element.remove();
            }
        }

        $('body').on('keydown', function (event) {
            i = 15;

            if (event.keyCode == 38) {
                if (playerMove >= 100) {

                    playerMove -= i++;
                    $('#player').css('top', playerMove + 'px');
                }
            }


            if (event.keyCode == 40) {
                if (playerMove <= (+(window).innerHeight) - ($('#player').height()) * 2) {
                    playerMove += i++;
                    $('#player').css('top', playerMove + 'px');
                }
            }
        });

        $('body').on('keydown', function (event) {
            if (event.keyCode == SPACE) {
                if (location.hash.slice(1) == "game") {
                    var b = new Bullet();
                    b.move();
                } else {
                    return;
                }
            }
        });

        $('#playGame').on('click', function () {
            setIntervalId = setInterval(function () {
                var enemy = new Enemy();
                enemyList.push(enemy);
                enemy.move();
            }, SPAWN_TIME);
            $(this).prop('disabled', true);
            $(this).hide();
            $('.nav-btn').css('cursor', 'not-allowed');
        });

        $(window).on('hashchange', function () {
            if (enemyList) {
                enemyList.forEach(z => z.die());
                enemyList.length = 0;
                clearInterval(setIntervalId);
            }
            location.reload();
            $('.bullets').remove();
        });
    }
    return gameController;
})();