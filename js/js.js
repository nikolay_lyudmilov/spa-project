var cars = [];

class Content {
    constructor(what, imageUrl, type) {
        this.what = what;
        this.imageUrl = imageUrl
        this.type = type;
    }
}

cars.push(new Content('nature', "assets/images/1.jpg", "Traffic"));
cars.push(new Content('nature', "assets/images/2.jpg", "People"));
cars.push(new Content('nature', "assets/images/4.jpg", "Hobby"));
cars.push(new Content('nature', "assets/images/5.jpg", "Work"));
cars.push(new Content('cars', "assets/images/5.jpg", "Work"));
cars.push(new Content('cars', "assets/images/6.jpg", "Work"));
cars.push(new Content('cars', "assets/images/7.jpg", "People"));
cars.push(new Content('cars', "assets/images/8.jpg", "Tasks"));
cars.push(new Content('people', "assets/images/4.jpg", "Hobby"));
cars.push(new Content('people', "assets/images/3.jpg", "Nature"));
cars.push(new Content('people', "assets/images/8.jpg", "Work"));
cars.push(new Content('people', "assets/images/2.jpg", "People"));