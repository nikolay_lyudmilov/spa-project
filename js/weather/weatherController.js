document.addEventListener('DOMContentLoaded', function () {
    var cities = ['Sofia', 'Varna', 'Burgas', 'Plovdiv', 'London', 'Rome', 'Paris', 'Berlin'],
        content = document.querySelector('#weather-template').innerHTML,
        template = Handlebars.compile(content),
        index = 1,
        flag = false;

    function setCity(city) {
        var c = city;
        weatherStorage.getWeather(c).then(function (data) {
            var icon = data.weather[0].main;
            var icon = data.weather[0].main;

            var tempC = Math.floor(data.main.temp - 273);
            document.querySelector("#temp-degree").innerHTML = `<p> ${tempC} °C </p>`;
            document.querySelector("#temp-result").innerHTML = template(data);
            weatherStorage.setIcons(document.querySelector('.skycons-icons'), icon)
        }).catch(function (err) {
            document.querySelector('#temp-result').innerHTML = '';
            document.querySelector("#temp-result").innerHTML = '<p style="color:red">The following error occurred ' + err + '</p>'
        });
    }

    setCity('Sofia');

    setInterval(() => {
        if (index == cities.length) {
            index = 0;
            flag = true;
        }

        if (flag) {
            var object = weatherStorage.getWeatherFromStorage(cities[index++]);
            var icon = object.weather[0].main;
            var icon = object.weather[0].main;

            var tempC = Math.floor(object.main.temp - 273);
            document.querySelector("#temp-degree").innerHTML = `<p> ${tempC} °C </p>`;
            document.querySelector('#temp-result').innerHTML = template(object);
            weatherStorage.setIcons(document.querySelector('.skycons-icons'), icon)
        } else {
            setCity(cities[index++]);
        }

    }, 10000);
});