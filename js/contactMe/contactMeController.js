function contactMeController() {
    $('.home_page').hide();
    $('.game_page').hide();
    $('.portfolio_page').hide();
    $('.contacts_page').show();
    $('.sponsors').show();
    $('.pages_page').hide();

    $(function () {
        $('#contacts-btn-submit').on('click', function (event) {
            var name = $('input[name=your-name]').val().trim();
            var email = $('input[name=your-email]').val().trim();
            var message = $('textarea[name=your-message]').val().trim();
            var statusEl = $('.status');
            
            statusEl.empty();

            function validate(type, typeStr, num) {
                if (!(type.length > num)) {
                    event.originalEvent.preventDefault();
                    statusEl.append(`<div>${typeStr} is too short</div>`);
                }
            }
            validate(name, "Name", 3);

            if (!(contactMeService.validateEmail(email))) {
                event.originalEvent.preventDefault();
                statusEl.append('<div>Email is not valid</div>');
            }

            validate(message, "Message", 10);
        });
    })
}