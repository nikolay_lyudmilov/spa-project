document.addEventListener('DOMContentLoaded', function () {
    function router() {
        var page = location.hash.slice(1);
        
        switch (page) {
            case 'home':
                homeController();
                break;
            case 'game':
                gameController();
                break;
            case 'pages':
                pagesController();
                break;
            case 'portfolio':
                portfolioController();
                break;
            case 'contacts':
                contactMeController();
                break;
            default:
                homeController();
                break;
        }
    }
    window.addEventListener('hashchange', router);
    router();
})